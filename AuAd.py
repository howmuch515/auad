#!/usr/bin/env python3
#-*- coding: utf-8 -*-
from tkinter import *
import sys
import sqlite3
import Interactive

class InputW(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        #data
        self.Title = StringVar()
        self.URL = StringVar()
        self.Account_ID = StringVar()
        self.Password = StringVar()
        self.Real_Name = StringVar()
        self.Email = StringVar()
        self.DataList = [self.Title, self.URL, self.Account_ID, self.Password, self.Real_Name, self.Email]
        #locating
        self.Width = 50
        self.Pad = 5
        self.Locating()
    
    def Locating(self):
        #Label
        for h, i in enumerate(["Title", "URL", "Account_ID", "Password", "Real_Name", "Email"]):
            Label(self, text=i).grid(row=h, column=0)
        #Entry
        for h, i in enumerate(self.DataList):
            Entry(self, textvariable=i, width=self.Width).grid(row=h, column=1, padx=self.Pad, pady=self.Pad)
        #Button
        Button(self, text="登録", command=self.Registration).grid(row=6, column=0, columnspan=2, padx=self.Pad, pady=self.Pad)
        self.pack()

    def Registration(self):
        with sqlite3.connect("./Authority.db") as db:
            cur = db.cursor()
            cur.execute("""insert into authority values(?, ?, ?, ?, ?, ?);""", [i.get() for i in self.DataList])
        sys.exit()
        
if __name__ == '__main__':
    argvs = sys.argv
    argc = len(argvs)
    
    #graphic-mode
    if argvs[1] == "-g":
        inputw = InputW()
        inputw.mainloop()

    #interactive-mode
    elif argvs[1] == "-i":
        Interactive.interactive()

    #direct-mode
    elif argvs[1] == "-d":
        with sqlite3.connect("./Authority.db") as db:
            cur = db.cursor()
            cur.execute("""insert into authority values(?, ?, ?, ?, ?, ?);""", [argvs[i] for i in range(2, 8)])
            
    #error 
    else: raise Exception("引数が不当です.")
